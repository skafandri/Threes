ansible-galaxy install -r roles.yml -p playbooks/roles
ansible-playbook -i hosts.ini playbooks/all.yml -e app_name=game
ansible-playbook -i hosts.ini playbooks/deploy.yml -e app_name=game
