apt-get update
apt-get install -y git python-yaml python-jinja2 python-pycurl python-pip python-dev

pip install docker-py

ansible-galaxy install -r roles.yml -p playbooks/roles

ansible-playbook playbooks/dev.yml --connection=local -e app_name=game
ansible-playbook -i hosts.ini playbooks/all.yml -e app_name=game
