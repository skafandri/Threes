<?php

namespace Threes;

class GameBoardFactory
{
    /**
     * @return BoardInterface
     */
    public function create()
    {
        $board = new Board();
        $grid = $board->getGrid();

        $counts = [
            1 => rand(1, 7),
        ];
        $counts[2] = rand(1, 9 - $counts[1] - 1);
        $counts[3] = 9 - ($counts[1] + $counts[2]);

        foreach ($counts as $value => $count) {
            while ($count-- > 0) {
                do {
                    $c = rand(0, 3);
                    $l = rand(0, 3);
                    $exist = $grid[$l][$c] > 0;
                    !$exist && $grid[$l][$c] = $value;
                } while ($exist);
            }
        }

        $board->setGrid($grid);
        return $board;
    }
}
