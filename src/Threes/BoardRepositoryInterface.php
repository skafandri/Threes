<?php

namespace Threes;

interface BoardRepositoryInterface
{
    /**
     * @return BoardInterface|null
     */
    public function findLastCreatedBoard();

    /**
     * @param BoardInterface $board
     * @return void
     */
    public function save(BoardInterface $board);

    /**
     * @param $limit number of boards to return
     * @return BoardInterface[]
     */
    public function findLastBoards($limit);

}
