<?php

namespace Threes;

class GameHistory
{
    /** @var  BoardRepositoryInterface */
    private $boardRepository;

    /**
     * GameHistory constructor.
     * @param BoardRepositoryInterface $boardRepository
     */
    public function __construct(BoardRepositoryInterface $boardRepository)
    {
        $this->boardRepository = $boardRepository;
    }

    public function getLastGames($limit = 10)
    {
        return $this->boardRepository->findLastBoards($limit);
    }
}
