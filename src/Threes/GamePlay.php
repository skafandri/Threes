<?php

namespace Threes;

class GamePlay
{
    /** @var  BoardRepositoryInterface */
    private $boardRepository;
    /** @var  GameBoardFactory */
    private $gameBoardFactory;

    /**
     * Game constructor.
     * @param BoardRepositoryInterface $boardRepository
     * @param GameBoardFactory $gameBoardFactory
     */
    public function __construct(BoardRepositoryInterface $boardRepository, GameBoardFactory $gameBoardFactory)
    {
        $this->boardRepository = $boardRepository;
        $this->gameBoardFactory = $gameBoardFactory;
    }

    public function restart()
    {
        $newBoard = $this->gameBoardFactory->create();
        $this->boardRepository->save($newBoard);
    }

    public function getGrid()
    {
        return $this->getActiveBoard()->getGrid();
    }

    public function play($action)
    {
        $board = $this->getActiveBoard();
        $board->move($action);
        $this->boardRepository->save($board);
    }

    private function getActiveBoard()
    {
        $board = $this->boardRepository->findLastCreatedBoard();
        if (!$board) {
            $board = $this->gameBoardFactory->create();
            $this->boardRepository->save($board);
        }
        return $board;
    }
}
