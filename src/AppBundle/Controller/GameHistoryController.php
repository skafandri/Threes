<?php

namespace AppBundle\Controller;

use AppBundle\FormType\GridType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Threes\GamePlay;

class GameHistoryController extends Controller
{
    /**
     * @Route("/history", name="history_index")
     */
    public function indexAction(Request $request)
    {
        $game = $this->get('app.history');
        $games = $game->getLastGames(10);

        return $this->render(
            'game/history.html.twig',
            [
                'games' => $games
            ]
        );
    }
}
