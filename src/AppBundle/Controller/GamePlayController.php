<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Board;
use AppBundle\FormType\GridType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GamePlayController extends Controller
{
    /**
     * @Route("/game/new", name="game_new")
     */
    public function newAction(Request $request)
    {
        $game = $this->get('app.game');
        $game->restart();

        $form = $this->createGridForm($game->getGrid());

        return $this->render(
            'game/new.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/game/play/{id}", name="game_play", defaults={"id": 0},)
     * @ParamConverter("board", class="AppBundle:Board")
     */
    public function playAction(Request $request, Board $board = null)
    {
        $game = $this->get('app.game');

        if (null === $board) {
            $board = $this->get('app.game')->getGrid();
        }

        $form = $this->createGridForm($board);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $game->play($form->getClickedButton()->getName());
            $form = $this->createGridForm($board->getGrid());
        }

        return $this->render(
            'game/play.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @param Board $board
     * @return \Symfony\Component\Form\Form
     */
    private function createGridForm($grid)
    {
        return $this->createForm(
            GridType::class,
            $grid,
            [
                'action' => $this->generateUrl('game_play'),
                'actions' => ['up', 'down', 'left', 'right']
            ]
        );
    }
}
