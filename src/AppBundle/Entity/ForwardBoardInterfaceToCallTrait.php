<?php

namespace AppBundle\Entity;

trait ForwardBoardInterfaceToCallTrait
{
    /**
     * @inheritdoc
     */
    public function getGrid()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritdoc
     */
    public function setGrid(array $grid)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritdoc
     */
    public function move($direction)
    {
        $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritdoc
     */
    public function getValidMoves()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritdoc
     */
    public function isOver()
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }
}