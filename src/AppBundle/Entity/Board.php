<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Threes\BoardInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BoardRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Board implements BoardInterface
{
    use ForwardBoardInterfaceToCallTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
    /**
     * @ORM\Column(type="json_array", name="grid")
     */
    private $grid;
    /**
     * @ORM\Column(type="string")
     */
    private $boardClass;
    /**
     * @ORM\Column(type="boolean")
     */
    private $gameOver = false;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createTime;

    /**
     * @var  BoardInterface
     */
    private $board;

    /**
     * Board constructor.
     * @param BoardInterface $board
     */
    public function __construct(BoardInterface $board)
    {
        $this->board = $board;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createTime = new \DateTime();
    }

    /**
     * @ORM\PreFlush
     */
    public function preFlush()
    {
        $this->grid = $this->board->getGrid();
        $this->gameOver = $this->board->isOver();
        $this->boardClass = get_class($this->board);
    }

    /**
     * @ORM\PostLoad
     */
    public function postLoad()
    {
        $this->board = new $this->boardClass();
        $this->board->setGrid($this->grid);
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->board, $name], $arguments);
    }

    /**
     * @return mixed
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }
}
