<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class GamePlayControllerTest extends WebTestCase
{

    public function test_html_table()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/game/new');
        $table = $crawler->filter('table.grid');

        $this->assertEquals(1, $table->count(), 'No grid table found');

        $trs = $table->filter('tr');
        $this->assertEquals(4, $trs->count(), 'Wrong TRs count');

        $trs->each(function (Crawler $crawler) {
            $this->assertEquals(4, $crawler->filter('td')->count(), 'Wrong TDs count');
        });
    }

    public function test_new_grid_each_time()
    {
        $client = static::createClient();
        $grid1 = $client->request('GET', '/game/new')->filter('table.grid')->text();
        $grid2 = $client->request('GET', '/game/new')->filter('table.grid')->text();
        $grid3 = $client->request('GET', '/game/new')->filter('table.grid')->text();

        $this->assertFalse(($grid1 == $grid2) && ($grid2 == $grid3));
    }

    public function test_game_buttons()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/game/new');

        $this->assertEquals('/game/play', $crawler->filter('form')->attr('action'));

        $buttons = [];

        $crawler->filter('form button')->each(function (Crawler $crawler) use (&$buttons) {
            $buttons[] = strtolower($crawler->text());
        });

        $this->assertArraySubset(['up', 'down', 'left', 'right'], $buttons);
    }

    public function test_play()
    {
        $client = static::createClient();
        $grid1 = $client->request('GET', '/game/play')->filter('table.grid')->text();
        $grid2 = $client->request('GET', '/game/play')->filter('table.grid')->text();

        $this->assertEquals($grid1, $grid2);
    }
}
