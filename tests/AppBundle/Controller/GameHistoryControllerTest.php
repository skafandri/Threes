<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class GameControllerTest extends WebTestCase
{

    public function test_list()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/history');


        $playLink = $crawler->filter('a')->links()[0]->getUri();

        $this->assertRegExp('/http:\/\/localhost\/game\/play\/\d/', $playLink);
    }
}
